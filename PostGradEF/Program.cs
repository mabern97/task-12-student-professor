﻿using System;

using PostGradEF.Contexts;
using PostGradEF.Models;
using PostGradEF.CRUD;
using Microsoft.EntityFrameworkCore.Internal;
using PostGradEF.Helpers;

namespace PostGradEF
{
    class Program
    {
        static PostGradCRUD crud;

        static void Main(string[] args)
        {
            crud = new PostGradCRUD();

            Console.WriteLine("Post Graduate Database Manager\n");

            Student s = new Student();

            ListStudents();
            ListProfessors();
            ListRelationships();
        }

        static void ListStudents()
        {
            // Students
            Console.WriteLine("\nListing all students:");
            foreach (Student student in crud.GetStudents())
                Console.WriteLine($"\t{student.FullName}");
        }

        static void ListProfessors()
        {
            // Professors
            Console.WriteLine("\nListing all professors:");
            foreach (Professor professor in crud.GetProfessors())
                Console.WriteLine($"\t{professor.FullName}");
        }

        static void ListRelationships()
        {
            // Student Professor Relations
            Console.WriteLine("\nListing all student professor relationships:");
            foreach (StudentProfessor relationship in crud.GetRelationships())
                Console.WriteLine($"\t{relationship.Professor.FullName} -  {relationship.Student.FullName}");
        }
    }
}
