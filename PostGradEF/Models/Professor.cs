﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PostGradEF.Models
{
    public class Professor
    {
        public int Id { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime TeachingSince { get; set; }

        public string FullName
        {
            get
            {
                return $"{FirstName} {LastName}";
            }
        }

        public ICollection<StudentProfessor> HasStudents { get; set; }

        public Professor()
        {
            HasStudents = new List<StudentProfessor>();
        }
    }
}
