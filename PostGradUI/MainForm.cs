﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using PostGradEF.CRUD;
using PostGradEF.Helpers;
using PostGradEF.Models;

namespace PostGradUI
{
    public partial class MainForm : Form
    {
        Dictionary<TreeNode, Student> students;
        Dictionary<TreeNode, Professor> professors;

        PostGradCRUD crud;

        OverviewManager overviewManager;
        TreeManager treeManager;

        TreeManager.NodeType entityType;
        object selectedEntity;

        StudentProfessor selectedRelation;

        RelationForm relationForm;

        public MainForm()
        {
            students = new Dictionary<TreeNode, Student>();
            professors = new Dictionary<TreeNode, Professor>();

            crud = new PostGradCRUD();
            InitializeComponent();

            relationForm = new RelationForm(crud);

            treeManager = new TreeManager(treeView1);
            RetrieveData();

            overviewManager = new OverviewManager(this);
            overviewManager.ResetOverview();

            // Bind events
            treeManager.onEntitySelect += TreeManager_onEntitySelect;

            overviewManager.onEntityUpdate += OverviewManager_onEntityUpdate;
            overviewManager.onEntityDelete += OverviewManager_onEntityDelete;

            overviewManager.onRelationClick += OverviewManager_onRelationClick;
            overviewManager.onRelationAdd += OverviewManager_onRelationAdd;
            overviewManager.onRelationDelete += OverviewManager_onRelationDelete;

            relationForm.onRelationshipConfirm += RelationForm_onRelationshipConfirm;

            exportButton.Click += ExportButton_Click;
        }

        private void ExportButton_Click(object sender, EventArgs e)
        {
            string directory = Directory.GetCurrentDirectory();

            SaveFileDialog dialog = new SaveFileDialog();
            dialog.FileName = "exported_PostGrad";
            dialog.InitialDirectory = directory;
            dialog.Filter = "JSON Document (*.json)|*.json";

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                Dictionary<string, object> data = new Dictionary<string, object>();
                data.Add("Students", students.Values);
                data.Add("Professors", professors.Values);

                PostGradExporter.ExportPostGradToJson(dialog.FileName, data);
                MessageBox.Show($"Successfully exported to '{dialog.FileName}'.");
                dialog.Dispose();
            }
        }

        private void OverviewManager_onEntityUpdate(object sender, EventArgs e)
        {
            switch (entityType)
            {
                case TreeManager.NodeType.Student:
                    Student student = selectedEntity as Student;

                    student.FirstName = firstNameField.Text;
                    student.LastName = lastNameField.Text;
                    student.DOB = dateField.Value;

                    crud.UpdateStudent(student);
                    RetrieveData();
                    overviewManager.DisplayStudent(student);
                    break;
                case TreeManager.NodeType.Professor:
                    Professor professor = selectedEntity as Professor;

                    professor.FirstName = firstNameField.Text;
                    professor.LastName = lastNameField.Text;
                    professor.TeachingSince = dateField.Value;

                    crud.UpdateProfessor(professor);
                    RetrieveData();
                    overviewManager.DisplayProfessor(professor);
                    break;
            }
        }

        private void OverviewManager_onEntityDelete(object sender, EventArgs e)
        {
            switch (entityType)
            {
                case TreeManager.NodeType.Student:
                    Student student = selectedEntity as Student;

                    crud.DeleteStudent(student);
                    break;
                case TreeManager.NodeType.Professor:
                    Professor professor = selectedEntity as Professor;

                    crud.DeleteProfessor(professor);
                    break;
            }

            overviewManager.ResetOverview();
            RetrieveData();
        }

        private void RelationForm_onRelationshipConfirm(object sender, EventArgs e)
        {
            List<object> relations = sender as List<object>;

            foreach(object relation in relations)
            {
                switch (entityType)
                {
                    case TreeManager.NodeType.Student:
                        Professor professor = relation as Professor;
                        crud.AddRelation(selectedEntity as Student, professor);

                        selectedEntity = crud.GetStudent((selectedEntity as Student).Id);

                        overviewManager.DisplayStudent(selectedEntity as Student);
                        overviewManager.DisplayRelationship(OverviewManager.RelationshipType.HasProfessors, (selectedEntity as Student).HasProfessors);
                        break;
                    case TreeManager.NodeType.Professor:
                        Student student = relation as Student;

                        crud.AddRelation(student, selectedEntity as Professor);

                        selectedEntity = crud.GetProfessor((selectedEntity as Professor).Id);

                        overviewManager.DisplayProfessor(selectedEntity as Professor);
                        overviewManager.DisplayRelationship(OverviewManager.RelationshipType.HasStudents, (selectedEntity as Professor).HasStudents);
                        break;
                }
            }
        }

        private void OverviewManager_onRelationDelete(object sender, EventArgs e)
        {
            crud.DeleteRelation(selectedRelation);

            switch (entityType)
            {
                case TreeManager.NodeType.Student:
                    selectedEntity = crud.GetStudent((selectedEntity as Student).Id);

                    Student student = selectedEntity as Student;

                    MessageBox.Show($"Removed '{selectedRelation.Professor.FullName}' as {student.FullName}'s professor.");
                    overviewManager.DisplayStudent(student);
                    overviewManager.DisplayRelationship(OverviewManager.RelationshipType.HasProfessors, student.HasProfessors);
                    break;
                case TreeManager.NodeType.Professor:
                    selectedEntity = crud.GetProfessor((selectedEntity as Professor).Id);

                    Professor professor = selectedEntity as Professor;

                    MessageBox.Show($"Removed '{selectedRelation.Student.FullName}' as {professor.FullName}'s student.");
                    overviewManager.DisplayProfessor(professor);
                    overviewManager.DisplayRelationship(OverviewManager.RelationshipType.HasStudents, professor.HasStudents);
                    break;
            }

            selectedRelation = null;
            relationListBox.ClearSelected();
        }

        private void OverviewManager_onRelationAdd(object sender, EventArgs e)
        {
            //MessageBox.Show(selectedRelation.ProfessorId.ToString() + " x " + selectedRelation.StudentId.ToString());
            relationForm.AddRelations(entityType, selectedEntity);
        }

        private void OverviewManager_onRelationClick(object sender, EventArgs e)
        {
            int relationIndex = Convert.ToInt32(sender);
            StudentProfessor relation = null;

            switch (entityType)
            {
                case TreeManager.NodeType.Student:
                    relation = (selectedEntity as Student).HasProfessors.ToList()[relationIndex];
                    break;
                case TreeManager.NodeType.Professor:
                    relation = (selectedEntity as Professor).HasStudents.ToList()[relationIndex];
                    break;
            }

            selectedRelation = relation;
        }

        private void TreeManager_onEntitySelect(object sender, EventArgs e)
        {
            TreeNode node = sender as TreeNode;

            if (students.ContainsKey(node))
            {
                Student student = students[node];

                selectedEntity = student;
                entityType = TreeManager.NodeType.Student;

                overviewManager.DisplayStudent(selectedEntity as Student);
            }
            else if (professors.ContainsKey(node))
            {
                Professor professor = professors[node];

                selectedEntity = professor;
                entityType = TreeManager.NodeType.Professor;

                overviewManager.DisplayProfessor(selectedEntity as Professor);
            }
        }

        public void RetrieveData()
        {
            students.Clear();
            professors.Clear();
            treeManager.Clear();

            // Add Students
            foreach (Student student in crud.GetStudents())
            {
                TreeNode node = treeManager.AddNode(student.FullName, TreeManager.NodeType.Student);
                students.Add(node, student);
            }

            // Add Professors
            foreach (Professor professor in crud.GetProfessors())
            {
                TreeNode node = treeManager.AddNode(professor.FullName, TreeManager.NodeType.Professor);
                professors.Add(node, professor);
            }
        }

    }
}
