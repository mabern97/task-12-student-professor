USE [master]
GO
/****** Object:  Database [PostGradDB]    Script Date: 8/28/2020 9:42:18 AM ******/
CREATE DATABASE [PostGradDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'PostGradDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\PostGradDB.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'PostGradDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\PostGradDB_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [PostGradDB] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [PostGradDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [PostGradDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [PostGradDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [PostGradDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [PostGradDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [PostGradDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [PostGradDB] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [PostGradDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [PostGradDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [PostGradDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [PostGradDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [PostGradDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [PostGradDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [PostGradDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [PostGradDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [PostGradDB] SET  ENABLE_BROKER 
GO
ALTER DATABASE [PostGradDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [PostGradDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [PostGradDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [PostGradDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [PostGradDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [PostGradDB] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [PostGradDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [PostGradDB] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [PostGradDB] SET  MULTI_USER 
GO
ALTER DATABASE [PostGradDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [PostGradDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [PostGradDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [PostGradDB] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [PostGradDB] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [PostGradDB] SET QUERY_STORE = OFF
GO
USE [PostGradDB]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 8/28/2020 9:42:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Professors]    Script Date: 8/28/2020 9:42:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Professors](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](max) NULL,
	[LastName] [nvarchar](max) NULL,
	[TeachingSince] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Professors] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StudentProfessors]    Script Date: 8/28/2020 9:42:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StudentProfessors](
	[StudentId] [int] NOT NULL,
	[ProfessorId] [int] NOT NULL,
 CONSTRAINT [PK_StudentProfessors] PRIMARY KEY CLUSTERED 
(
	[StudentId] ASC,
	[ProfessorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Students]    Script Date: 8/28/2020 9:42:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Students](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](max) NULL,
	[LastName] [nvarchar](max) NULL,
	[DOB] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Students] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200828073610_PostGradDB', N'3.1.7')
GO
SET IDENTITY_INSERT [dbo].[Professors] ON 

INSERT [dbo].[Professors] ([Id], [FirstName], [LastName], [TeachingSince]) VALUES (1, N'Nicolas', N'Lennox', CAST(N'1982-08-28T09:36:10.6008421' AS DateTime2))
INSERT [dbo].[Professors] ([Id], [FirstName], [LastName], [TeachingSince]) VALUES (2, N'Dewald', N'Els', CAST(N'1980-08-28T09:36:10.6032718' AS DateTime2))
INSERT [dbo].[Professors] ([Id], [FirstName], [LastName], [TeachingSince]) VALUES (3, N'Dean', N'von Schoultz', CAST(N'1980-08-28T09:36:10.6032759' AS DateTime2))
SET IDENTITY_INSERT [dbo].[Professors] OFF
GO
INSERT [dbo].[StudentProfessors] ([StudentId], [ProfessorId]) VALUES (1, 1)
INSERT [dbo].[StudentProfessors] ([StudentId], [ProfessorId]) VALUES (2, 2)
INSERT [dbo].[StudentProfessors] ([StudentId], [ProfessorId]) VALUES (3, 3)
INSERT [dbo].[StudentProfessors] ([StudentId], [ProfessorId]) VALUES (4, 3)
GO
SET IDENTITY_INSERT [dbo].[Students] ON 

INSERT [dbo].[Students] ([Id], [FirstName], [LastName], [DOB]) VALUES (1, N'Mathias', N'Berntsen', CAST(N'1997-08-28T09:36:10.6035243' AS DateTime2))
INSERT [dbo].[Students] ([Id], [FirstName], [LastName], [DOB]) VALUES (2, N'Nicholas', N'Anderson', CAST(N'1993-08-28T09:36:10.6035771' AS DateTime2))
INSERT [dbo].[Students] ([Id], [FirstName], [LastName], [DOB]) VALUES (3, N'André', N'Ottosen', CAST(N'1992-08-28T09:36:10.6035793' AS DateTime2))
INSERT [dbo].[Students] ([Id], [FirstName], [LastName], [DOB]) VALUES (4, N'Johannes', N'Wathne', CAST(N'1997-08-28T09:36:10.6035797' AS DateTime2))
SET IDENTITY_INSERT [dbo].[Students] OFF
GO
/****** Object:  Index [IX_StudentProfessors_ProfessorId]    Script Date: 8/28/2020 9:42:19 AM ******/
CREATE NONCLUSTERED INDEX [IX_StudentProfessors_ProfessorId] ON [dbo].[StudentProfessors]
(
	[ProfessorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[StudentProfessors]  WITH CHECK ADD  CONSTRAINT [FK_StudentProfessors_Professors_ProfessorId] FOREIGN KEY([ProfessorId])
REFERENCES [dbo].[Professors] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[StudentProfessors] CHECK CONSTRAINT [FK_StudentProfessors_Professors_ProfessorId]
GO
ALTER TABLE [dbo].[StudentProfessors]  WITH CHECK ADD  CONSTRAINT [FK_StudentProfessors_Students_StudentId] FOREIGN KEY([StudentId])
REFERENCES [dbo].[Students] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[StudentProfessors] CHECK CONSTRAINT [FK_StudentProfessors_Students_StudentId]
GO
USE [master]
GO
ALTER DATABASE [PostGradDB] SET  READ_WRITE 
GO
