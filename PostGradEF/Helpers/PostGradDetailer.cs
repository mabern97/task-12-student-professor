﻿using System;
using System.Collections.Generic;
using System.Text;

using PostGradEF.Models;

namespace PostGradEF.Helpers
{
    public class PostGradDetailer
    {
        public static string PrintStudent(Student student)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("\nStudent {\n");
            sb.Append($"\tFull Name: {student.FullName}\n"); // Student's full name
            sb.Append($"\tFirst Name: {student.FirstName}\n"); // Student's full name
            sb.Append($"\tLast Name: {student.LastName}\n"); // Student's last name
            sb.Append($"\tDate of Birth: {student.DOB}\n"); // Student's DOB
            // Show professor relationships
            sb.Append("\tProfessors: {\n");
            foreach (StudentProfessor professor in student.HasProfessors)
            {
                sb.Append($"\t\t{professor.Professor.FullName}\n");
            }
            sb.Append("}\n");

            return sb.ToString();
        }

        public static string PrintProfessor(Professor professor)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("\nProfessor {\n");
            sb.Append($"\tFull Name: {professor.FullName}\n"); // Professor's full name
            sb.Append($"\tFirst Name: {professor.FirstName}\n"); // Professor's full name
            sb.Append($"\tLast Name: {professor.LastName}\n"); // Professor's last name
            sb.Append($"\tLecturing Since: {professor.TeachingSince}\n"); // Professor's tenure
            // Show professor relationships
            sb.Append("\tStudents: {\n");
            foreach (StudentProfessor student in professor.HasStudents)
            {
                sb.Append($"\t\t{student.Student.FullName}\n");
            }
            sb.Append("}\n");

            return sb.ToString();
        }
    }
}
