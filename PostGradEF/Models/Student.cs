﻿using System;
using System.Collections.Generic;

namespace PostGradEF.Models
{
    public class Student
    {
        // Primary Key
        public int Id { get; set; }
        // Fields
        public string FirstName { get; set; } // First Name
        public string LastName { get; set; } // Surname
        public DateTime DOB { get; set; } // Date of Birth

        public string FullName
        {
            get
            {
                return $"{FirstName} {LastName}";
            }
        }

        public ICollection<StudentProfessor> HasProfessors { get; set; }

        public Student()
        {
            HasProfessors = new List<StudentProfessor>();
        }
    }
}
