Type:    Windows Form or Console Application

•Allow a university post grad administrator to assign Supervisors (Professors) to Students to supervise them during their postgrad research degrees.

Requirements:

•Use a code first workflow to create the initial migrations and the corresponding database using entity framework (DBContext)
•Can either be a one to one relationship (A professor can only supervise one student) OR a one to many (A professor can supervise many students and a student can only have one professor)
•Navigation properties
•Must demonstrate adding these entities to the database
•This means just migrations, not adding data.
•Purpose: Learn Entity Framework: Migrations

Weight: Necessary

Pt2 Additions:
Type:     Windows Form or Console Application

Upgrade your previous solution to include a one to many, many to many and one to one relationship of your choice. (If you included a one to one in the first version then add a one to many and vice versa)

Requirements:    Create the relevant methods to allow for CRUD operations – it is up to you which operations are available to the user depending on how you designed your relational model.

There must be at least 2 entities where CRUD operations are performed.

Add comments in the model classes explaining the navigation properties and FKs (i.e ProfId is a FK reference to the Professor entity, and the Professor object is the navigation property from student (dependent class) to professor (principle class)). The comments will be marked as part of functionality as it is important for you to understand what the conventions mean. 

Purpose: Learn Entity Framework: Relationships

Pt3 Additions

Add a method into your PGManager project to allow for the current DBSet of Supervisors/Professors to be serialized into JSON. 
You can either write the JSON to a text file within your debug folder or add a component to your UI to display the JSON text.

Purpose: Learn JSON usage within .NET.