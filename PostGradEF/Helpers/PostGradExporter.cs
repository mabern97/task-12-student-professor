﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using PostGradEF.Models;

namespace PostGradEF.Helpers
{
    public class PostGradExporter
    {
        public static string ExportPostGradToJson(string file, object entity)
        {
            using (StreamWriter stream = new StreamWriter(file))
            {
                //JsonSerializer json = new JsonSerializer();

                string data = JsonConvert.SerializeObject(entity, Formatting.Indented,
                    new JsonSerializerSettings
                    {
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                    });

                stream.Flush();
                stream.Write(data);
                stream.Close();
            }

            return "";
        }
    }
}
