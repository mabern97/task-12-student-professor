﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PostGradEF.Models
{
    public class StudentProfessor
    {
        /*
         * StudentID is a Foreign Key reference to the Student entity
         * The Student object is the navigation property from the Professor (dependent) to the Student (principle)
         * 
         * ProfessorID is a Foreign Key reference to the Professor entity
         * The Professor object is the navigation property from the Student (dependent) to the Professor (principle)
         */
        public int StudentId { get; set; } // FK reference to Student Entity
        public Student Student { get; set; } // Navigation Property (Dependent class)
        public int ProfessorId { get; set; } // FK reference to Professor Entity
        public Professor Professor { get; set; } // Navigation Property (Principle class)
    }
}
