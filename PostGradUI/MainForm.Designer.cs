﻿namespace PostGradUI
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listGroup = new System.Windows.Forms.GroupBox();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.overviewGroup = new System.Windows.Forms.GroupBox();
            this.relationGroup = new System.Windows.Forms.GroupBox();
            this.deleteRelationButton = new System.Windows.Forms.Button();
            this.addRelationButton = new System.Windows.Forms.Button();
            this.relationListBox = new System.Windows.Forms.ListBox();
            this.deleteButton = new System.Windows.Forms.Button();
            this.updateButton = new System.Windows.Forms.Button();
            this.exportButton = new System.Windows.Forms.Button();
            this.dateField = new System.Windows.Forms.DateTimePicker();
            this.dateLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lastNameField = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.firstNameField = new System.Windows.Forms.TextBox();
            this.fullNameField = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.listGroup.SuspendLayout();
            this.overviewGroup.SuspendLayout();
            this.relationGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // listGroup
            // 
            this.listGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listGroup.Controls.Add(this.treeView1);
            this.listGroup.Location = new System.Drawing.Point(342, 12);
            this.listGroup.Name = "listGroup";
            this.listGroup.Size = new System.Drawing.Size(254, 426);
            this.listGroup.TabIndex = 0;
            this.listGroup.TabStop = false;
            this.listGroup.Text = "Students and Professors";
            // 
            // treeView1
            // 
            this.treeView1.AllowDrop = true;
            this.treeView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.treeView1.HideSelection = false;
            this.treeView1.Location = new System.Drawing.Point(6, 22);
            this.treeView1.Name = "treeView1";
            this.treeView1.ShowPlusMinus = false;
            this.treeView1.ShowRootLines = false;
            this.treeView1.Size = new System.Drawing.Size(242, 398);
            this.treeView1.TabIndex = 0;
            // 
            // overviewGroup
            // 
            this.overviewGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.overviewGroup.Controls.Add(this.relationGroup);
            this.overviewGroup.Controls.Add(this.deleteButton);
            this.overviewGroup.Controls.Add(this.updateButton);
            this.overviewGroup.Controls.Add(this.exportButton);
            this.overviewGroup.Controls.Add(this.dateField);
            this.overviewGroup.Controls.Add(this.dateLabel);
            this.overviewGroup.Controls.Add(this.label3);
            this.overviewGroup.Controls.Add(this.lastNameField);
            this.overviewGroup.Controls.Add(this.label2);
            this.overviewGroup.Controls.Add(this.firstNameField);
            this.overviewGroup.Controls.Add(this.fullNameField);
            this.overviewGroup.Controls.Add(this.label1);
            this.overviewGroup.Location = new System.Drawing.Point(12, 12);
            this.overviewGroup.Name = "overviewGroup";
            this.overviewGroup.Size = new System.Drawing.Size(324, 426);
            this.overviewGroup.TabIndex = 1;
            this.overviewGroup.TabStop = false;
            this.overviewGroup.Text = "Overview";
            // 
            // relationGroup
            // 
            this.relationGroup.Controls.Add(this.deleteRelationButton);
            this.relationGroup.Controls.Add(this.addRelationButton);
            this.relationGroup.Controls.Add(this.relationListBox);
            this.relationGroup.Location = new System.Drawing.Point(6, 139);
            this.relationGroup.Name = "relationGroup";
            this.relationGroup.Size = new System.Drawing.Size(312, 252);
            this.relationGroup.TabIndex = 4;
            this.relationGroup.TabStop = false;
            this.relationGroup.Text = "Relations";
            // 
            // deleteRelationButton
            // 
            this.deleteRelationButton.Location = new System.Drawing.Point(124, 223);
            this.deleteRelationButton.Name = "deleteRelationButton";
            this.deleteRelationButton.Size = new System.Drawing.Size(112, 23);
            this.deleteRelationButton.TabIndex = 1;
            this.deleteRelationButton.Text = "Remove Relation";
            this.deleteRelationButton.UseVisualStyleBackColor = true;
            // 
            // addRelationButton
            // 
            this.addRelationButton.Location = new System.Drawing.Point(6, 223);
            this.addRelationButton.Name = "addRelationButton";
            this.addRelationButton.Size = new System.Drawing.Size(112, 23);
            this.addRelationButton.TabIndex = 1;
            this.addRelationButton.Text = "Add Relation";
            this.addRelationButton.UseVisualStyleBackColor = true;
            // 
            // relationListBox
            // 
            this.relationListBox.FormattingEnabled = true;
            this.relationListBox.ItemHeight = 15;
            this.relationListBox.Location = new System.Drawing.Point(6, 22);
            this.relationListBox.Name = "relationListBox";
            this.relationListBox.Size = new System.Drawing.Size(300, 199);
            this.relationListBox.TabIndex = 0;
            // 
            // deleteButton
            // 
            this.deleteButton.Location = new System.Drawing.Point(229, 397);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(89, 23);
            this.deleteButton.TabIndex = 3;
            this.deleteButton.Text = "Delete";
            this.deleteButton.UseVisualStyleBackColor = true;
            // 
            // updateButton
            // 
            this.updateButton.Location = new System.Drawing.Point(101, 397);
            this.updateButton.Name = "updateButton";
            this.updateButton.Size = new System.Drawing.Size(122, 23);
            this.updateButton.TabIndex = 3;
            this.updateButton.Text = "Update";
            this.updateButton.UseVisualStyleBackColor = true;
            // 
            // exportButton
            // 
            this.exportButton.Location = new System.Drawing.Point(6, 397);
            this.exportButton.Name = "exportButton";
            this.exportButton.Size = new System.Drawing.Size(89, 23);
            this.exportButton.TabIndex = 3;
            this.exportButton.Text = "Export All";
            this.exportButton.UseVisualStyleBackColor = true;
            // 
            // dateField
            // 
            this.dateField.Location = new System.Drawing.Point(112, 110);
            this.dateField.Name = "dateField";
            this.dateField.Size = new System.Drawing.Size(206, 23);
            this.dateField.TabIndex = 2;
            // 
            // dateLabel
            // 
            this.dateLabel.Location = new System.Drawing.Point(6, 109);
            this.dateLabel.Name = "dateLabel";
            this.dateLabel.Size = new System.Drawing.Size(100, 23);
            this.dateLabel.TabIndex = 0;
            this.dateLabel.Text = "Date of Birth:";
            this.dateLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(6, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 23);
            this.label3.TabIndex = 0;
            this.label3.Text = "Last Name: ";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lastNameField
            // 
            this.lastNameField.Location = new System.Drawing.Point(112, 81);
            this.lastNameField.Name = "lastNameField";
            this.lastNameField.Size = new System.Drawing.Size(206, 23);
            this.lastNameField.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(6, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 23);
            this.label2.TabIndex = 0;
            this.label2.Text = "First Name: ";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // firstNameField
            // 
            this.firstNameField.Location = new System.Drawing.Point(112, 52);
            this.firstNameField.Name = "firstNameField";
            this.firstNameField.Size = new System.Drawing.Size(206, 23);
            this.firstNameField.TabIndex = 1;
            // 
            // fullNameField
            // 
            this.fullNameField.Location = new System.Drawing.Point(112, 23);
            this.fullNameField.Name = "fullNameField";
            this.fullNameField.ReadOnly = true;
            this.fullNameField.Size = new System.Drawing.Size(206, 23);
            this.fullNameField.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(6, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "Full Name: ";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(608, 450);
            this.Controls.Add(this.overviewGroup);
            this.Controls.Add(this.listGroup);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Post Graduate Manager";
            this.listGroup.ResumeLayout(false);
            this.overviewGroup.ResumeLayout(false);
            this.overviewGroup.PerformLayout();
            this.relationGroup.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox listGroup;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox fullNameField;
        public System.Windows.Forms.Button deleteButton;
        public System.Windows.Forms.Button updateButton;
        public System.Windows.Forms.Button exportButton;
        public System.Windows.Forms.DateTimePicker dateField;
        public System.Windows.Forms.Label dateLabel;
        public System.Windows.Forms.TextBox lastNameField;
        public System.Windows.Forms.TextBox firstNameField;
        public System.Windows.Forms.GroupBox relationGroup;
        public System.Windows.Forms.Button addRelationButton;
        public System.Windows.Forms.ListBox relationListBox;
        public System.Windows.Forms.Button deleteRelationButton;
        public System.Windows.Forms.TreeView treeView1;
        public System.Windows.Forms.GroupBox overviewGroup;
    }
}

