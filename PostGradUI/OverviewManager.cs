﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using PostGradEF.Models;

namespace PostGradUI
{
    public class OverviewManager
    {
        public enum RelationshipType
        {
            HasProfessors,
            HasStudents,
        };

        // Main Overview
        private TextBox fullNameField;
        private TextBox firstNameField;
        private TextBox lastNameField;

        private GroupBox overviewGroup;
        private GroupBox relationGroup;

        private Label dateLabel;
        private DateTimePicker datePicker;

        private Button exportButton;
        private Button updateButton;
        private Button deleteButton;

        // Relation Overview
        private ListBox relationBox;

        private Button addRelationButton;
        private Button removeRelationButton;

        public event EventHandler onEntityUpdate;
        public event EventHandler onEntityDelete;

        public event EventHandler onRelationClick;
        public event EventHandler onRelationAdd;
        public event EventHandler onRelationDelete;

        protected void onEntityUpdated(EventArgs e)
        {
            EventHandler handler = onEntityUpdate;
            handler?.Invoke(this, e);
        }

        protected void onEntityDeleted(EventArgs e)
        {
            EventHandler handler = onEntityDelete;
            handler?.Invoke(this, e);
        }

        protected void onRelationClicked(EventArgs e)
        {
            EventHandler handler = onRelationClick;
            handler?.Invoke(this, e);
        }

        protected void onRelationAdded(EventArgs e)
        {
            EventHandler handler = onRelationAdd;
            handler?.Invoke(this, e);
        }

        protected void onRelationDeleted(EventArgs e)
        {
            EventHandler handler = onRelationDelete;
            handler?.Invoke(this, e);
        }

        public OverviewManager(MainForm form)
        {
            overviewGroup = form.overviewGroup;
            relationGroup = form.relationGroup;

            // Main Overview control assignment
            fullNameField = form.fullNameField;
            firstNameField = form.firstNameField;
            lastNameField = form.lastNameField;

            dateLabel = form.dateLabel;
            datePicker = form.dateField;

            exportButton = form.exportButton;

            updateButton = form.updateButton;
            updateButton.Enabled = false;

            deleteButton = form.deleteButton;
            deleteButton.Enabled = false;

            // Relation Overview control assignment
            relationBox = form.relationListBox;

            addRelationButton = form.addRelationButton;
            addRelationButton.Enabled = false;

            removeRelationButton = form.deleteRelationButton;
            removeRelationButton.Enabled = false;

            // Bind events
            relationBox.SelectedIndexChanged += OnRelationClick;

            updateButton.Click += (sender, e) =>
            {
                onEntityUpdate(null, null);
            };

            deleteButton.Click += (sender, e) =>
            {
                onEntityDelete(null, null);
            };

            addRelationButton.Click += (sender, e) =>
            {
                onRelationAdd(null, null);
            };

            removeRelationButton.Click += (sender, e) =>
            {
                onRelationDelete(null, null);
            };

            // Setup
            ResetOverview();
        }

        private void OnRelationClick(object sender, EventArgs e)
        {
            removeRelationButton.Enabled = true;

            onRelationClick(relationBox.SelectedIndex, null);
        }

        public void ResetOverview()
        {
            fullNameField.ResetText();
            firstNameField.ResetText();
            lastNameField.ResetText();

            dateLabel.Visible = false;
            datePicker.Visible = false;
            overviewGroup.Text = "Overview";
            relationGroup.Text = "Relations";

            updateButton.Enabled = false;
            deleteButton.Enabled = false;

            addRelationButton.Enabled = false;
            removeRelationButton.Enabled = false;

            datePicker.ResetText();
            relationBox.ResetText();
    }

        public void DisplayStudent(Student student)
        {
            // Change the group box names
            overviewGroup.Text = "Student Overview";
            relationGroup.Text = "List of Professors";

            dateLabel.Visible = true;
            datePicker.Visible = true;

            dateLabel.Text = "Date of Birth";

            updateButton.Enabled = true;
            deleteButton.Enabled = true;

            addRelationButton.Enabled = true;

            // Show the main information
            fullNameField.Text = student.FullName;
            firstNameField.Text = student.FirstName;
            lastNameField.Text = student.LastName;
            datePicker.Value = student.DOB;

            DisplayRelationship(RelationshipType.HasProfessors, student.HasProfessors);
        }

        public void DisplayProfessor(Professor professor)
        {
            // Change the group box names
            overviewGroup.Text = "Professor Overview";
            relationGroup.Text = "List of Students";

            dateLabel.Visible = true;
            datePicker.Visible = true;

            dateLabel.Text = "Date of Tenure";

            updateButton.Enabled = true;
            deleteButton.Enabled = true;

            addRelationButton.Enabled = true;

            // Show the main information
            fullNameField.Text = professor.FullName;
            firstNameField.Text = professor.FirstName;
            lastNameField.Text = professor.LastName;
            datePicker.Value = professor.TeachingSince;

            DisplayRelationship(RelationshipType.HasStudents, professor.HasStudents);
        }

        public void DisplayRelationship(RelationshipType type, IEnumerable<StudentProfessor> relationships)
        {
            relationBox.Items.Clear();
            
            switch (type)
            {
                case RelationshipType.HasProfessors:
                    foreach(StudentProfessor relation in relationships)
                        relationBox.Items.Add(relation.Professor.FullName);
                    break;
                case RelationshipType.HasStudents:
                    foreach (StudentProfessor relation in relationships)
                        relationBox.Items.Add(relation.Student.FullName);
                    break;
            }
        }
    }
}
