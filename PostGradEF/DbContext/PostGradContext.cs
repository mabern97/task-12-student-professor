﻿using System;
using Microsoft.EntityFrameworkCore;
using PostGradEF.CRUD;
using PostGradEF.Models;

namespace PostGradEF.Contexts
{
    public class PostGradContext : DbContext
    {
        public virtual DbSet<Student> Students { get; set; }
        public virtual DbSet<Professor> Professors { get; set; }

        public virtual DbSet<StudentProfessor> StudentProfessors { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.EnableSensitiveDataLogging(true);
            if (!optionsBuilder.IsConfigured)
            {
                string _connStr = @"Server=MATHIASBERN8FFA\SQLEXPRESS;Database=PostGradDB;Trusted_Connection=True;";
                //string _connStr = @"Database=InitialDB;User Id=SA;Password=reallyStrongPwd123";
                optionsBuilder.UseSqlServer(_connStr);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Composite Key
            modelBuilder.Entity<StudentProfessor>().HasKey(sp => new { sp.StudentId, sp.ProfessorId });

            Professor nicolas = new Professor() { Id = 1, FirstName = "Nicolas", LastName = "Lennox", TeachingSince = DateTime.Now.AddYears(-38) };
            Professor dewald = new Professor() { Id = 2, FirstName = "Dewald", LastName = "Els", TeachingSince = DateTime.Now.AddYears(-40) };
            Professor dean = new Professor() { Id = 3, FirstName = "Dean", LastName = "von Schoultz", TeachingSince = DateTime.Now.AddYears(-40) };

            Student mathias = new Student() { Id = 1, FirstName = "Mathias", LastName = "Berntsen", DOB = DateTime.Now.AddYears(-23) };
            Student nicholas = new Student() { Id = 2, FirstName = "Nicholas", LastName = "Anderson", DOB = DateTime.Now.AddYears(-27) };
            Student andre = new Student() { Id = 3, FirstName = "André", LastName = "Ottosen", DOB = DateTime.Now.AddYears(-28) };
            Student johannes = new Student() { Id = 4, FirstName = "Johannes", LastName = "Wathne", DOB = DateTime.Now.AddYears(-23) };

            modelBuilder.Entity<Professor>().HasData(nicolas);
            modelBuilder.Entity<Professor>().HasData(dewald);
            modelBuilder.Entity<Professor>().HasData(dean);

            modelBuilder.Entity<Student>().HasData(mathias);
            modelBuilder.Entity<Student>().HasData(nicholas);
            modelBuilder.Entity<Student>().HasData(andre);
            modelBuilder.Entity<Student>().HasData(johannes);

            modelBuilder.Entity<StudentProfessor>().HasData(new StudentProfessor() {
                ProfessorId = nicolas.Id,
                StudentId = mathias.Id
            });

            modelBuilder.Entity<StudentProfessor>().HasData(new StudentProfessor()
            {
                ProfessorId = dewald.Id,
                StudentId = nicholas.Id
            });

            modelBuilder.Entity<StudentProfessor>().HasData(new StudentProfessor()
            {
                ProfessorId = dean.Id,
                StudentId = andre.Id
            });

            modelBuilder.Entity<StudentProfessor>().HasData(new StudentProfessor()
            {
                ProfessorId = dean.Id,
                StudentId = johannes.Id
            });
        }
    }
}
