﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PostGradEF.Migrations
{
    public partial class PostGradDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Professors",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    TeachingSince = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Professors", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Students",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    DOB = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Students", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "StudentProfessors",
                columns: table => new
                {
                    StudentId = table.Column<int>(nullable: false),
                    ProfessorId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentProfessors", x => new { x.StudentId, x.ProfessorId });
                    table.ForeignKey(
                        name: "FK_StudentProfessors_Professors_ProfessorId",
                        column: x => x.ProfessorId,
                        principalTable: "Professors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StudentProfessors_Students_StudentId",
                        column: x => x.StudentId,
                        principalTable: "Students",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Professors",
                columns: new[] { "Id", "FirstName", "LastName", "TeachingSince" },
                values: new object[,]
                {
                    { 1, "Nicolas", "Lennox", new DateTime(1982, 8, 28, 9, 36, 10, 600, DateTimeKind.Local).AddTicks(8421) },
                    { 2, "Dewald", "Els", new DateTime(1980, 8, 28, 9, 36, 10, 603, DateTimeKind.Local).AddTicks(2718) },
                    { 3, "Dean", "von Schoultz", new DateTime(1980, 8, 28, 9, 36, 10, 603, DateTimeKind.Local).AddTicks(2759) }
                });

            migrationBuilder.InsertData(
                table: "Students",
                columns: new[] { "Id", "DOB", "FirstName", "LastName" },
                values: new object[,]
                {
                    { 1, new DateTime(1997, 8, 28, 9, 36, 10, 603, DateTimeKind.Local).AddTicks(5243), "Mathias", "Berntsen" },
                    { 2, new DateTime(1993, 8, 28, 9, 36, 10, 603, DateTimeKind.Local).AddTicks(5771), "Nicholas", "Anderson" },
                    { 3, new DateTime(1992, 8, 28, 9, 36, 10, 603, DateTimeKind.Local).AddTicks(5793), "André", "Ottosen" },
                    { 4, new DateTime(1997, 8, 28, 9, 36, 10, 603, DateTimeKind.Local).AddTicks(5797), "Johannes", "Wathne" }
                });

            migrationBuilder.InsertData(
                table: "StudentProfessors",
                columns: new[] { "StudentId", "ProfessorId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 2, 2 },
                    { 3, 3 },
                    { 4, 3 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_StudentProfessors_ProfessorId",
                table: "StudentProfessors",
                column: "ProfessorId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "StudentProfessors");

            migrationBuilder.DropTable(
                name: "Professors");

            migrationBuilder.DropTable(
                name: "Students");
        }
    }
}
