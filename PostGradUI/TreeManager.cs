﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace PostGradUI
{
    public class TreeManager
    {
        public enum NodeType
        {
            Student,
            Professor
        }

        private TreeView treeView;

        private Font boldFont;
        private Font regularFont;

        private TreeNode studentNode;
        private TreeNode professorNode;

        public event EventHandler onEntitySelect;

        protected virtual void onEntitySelected(EventArgs e)
        {
            EventHandler handler = onEntitySelect;
            handler?.Invoke(this, e);
        }

        public TreeManager(TreeView view)
        {
            boldFont = new Font(TreeView.DefaultFont, FontStyle.Bold);
            regularFont = new Font(TreeView.DefaultFont, FontStyle.Regular);

            treeView = view;
            treeView.Font = boldFont;
            Clear();

            treeView.NodeMouseClick += onNodeClick;
        }

        private void onNodeClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            onEntitySelect(e.Node, null);
        }

        public void Clear()
        {
            treeView.Nodes.Clear();

            studentNode = new TreeNode("Students");
            professorNode = new TreeNode("Professors");

            treeView.Nodes.Add(studentNode);
            treeView.Nodes.Add(professorNode);
        }

        public TreeNode AddNode(string name, NodeType type)
        {
            TreeNode node = new TreeNode(name);
            node.NodeFont = regularFont;

            switch(type)
            {
                case NodeType.Student:
                    studentNode.Nodes.Add(node);
                    break;
                case NodeType.Professor:
                    professorNode.Nodes.Add(node);
                    break;
            }

            treeView.ExpandAll();

            return node;
        }
    }
}
