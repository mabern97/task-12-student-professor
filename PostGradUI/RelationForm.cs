﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using PostGradEF.CRUD;
using PostGradEF.Models;

namespace PostGradUI
{
    public partial class RelationForm : Form
    {
        private PostGradCRUD crud;

        private TreeManager.NodeType entityType;
        private object selectedEntity;

        private List<object> options;

        public event EventHandler onRelationshipConfirm;

        protected void OnRelationshipConfirmed(EventArgs e)
        {
            EventHandler handler = onRelationshipConfirm;
            handler?.Invoke(this, e);
        }

        public RelationForm(PostGradCRUD crud)
        {
            this.crud = crud;
            InitializeComponent();

            options = new List<object>();

            button1.Click += onConfirm;
            button2.Click += onCancel;
        }

        private void onCancel(object sender, EventArgs e)
        {
            Close();
        }

        private void onConfirm(object sender, EventArgs e)
        {
            List<object> items = new List<object>();

            foreach(int checkedIndex in checkedListBox1.CheckedIndices)
            {
                items.Add(options[checkedIndex]);
            }

            Close();
            onRelationshipConfirm(items, null);
        }

        private void Reset()
        {
            selectedEntity = null;

            options.Clear();
            checkedListBox1.Items.Clear();
        }

        public void AddRelations(TreeManager.NodeType type, object to)
        {
            Reset();
            entityType = type;
            selectedEntity = to;

            switch (entityType)
            {
                case TreeManager.NodeType.Student:

                    groupBox1.Text = "Professors";
                    label1.Text = $"Adding relations for '{(selectedEntity as Student).FullName}'";

                    List<Professor> professors = crud.GetProfessors().ToList();

                    foreach(StudentProfessor sp in (selectedEntity as Student).HasProfessors)
                    {
                        professors.Remove(sp.Professor);
                    }

                    options.AddRange(professors);
                    

                    break;
                case TreeManager.NodeType.Professor:
                    groupBox1.Text = "Students";
                    label1.Text = $"Adding relations for '{(selectedEntity as Professor).FullName}'";

                    List<Student> students = crud.GetStudents().ToList();

                    foreach (StudentProfessor sp in (selectedEntity as Professor).HasStudents)
                    {
                        students.Remove(sp.Student);
                    }

                    options.AddRange(students);

                    break;
            }

            foreach(object option in options)
            {
                switch (entityType)
                {
                    case TreeManager.NodeType.Student:
                        Professor professor = option as Professor;
                        checkedListBox1.Items.Add(professor.FullName);
                        break;
                    case TreeManager.NodeType.Professor:
                        Student student = option as Student;
                        checkedListBox1.Items.Add(student.FullName);
                        break;
                }
            }

            ShowDialog();
        }
    }
}
