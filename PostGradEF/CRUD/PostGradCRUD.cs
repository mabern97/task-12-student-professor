﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Conventions;
using PostGradEF.Contexts;
using PostGradEF.Models;

namespace PostGradEF.CRUD
{
    public class PostGradCRUD
    {
        private PostGradContext context;

        public PostGradCRUD()
        {
            context = new PostGradContext();
        }

        public StudentProfessor AddRelation(Student student, Professor professor)
        {
            StudentProfessor composite = new StudentProfessor()
            {
                Student = student,
                Professor = professor
            };

            context.StudentProfessors.Add(composite);
            context.SaveChanges();

            return composite;
        }

        #region CREATE
        public Student AddStudent(Student student)
        {
            context.Students.Add(student);
            context.SaveChanges();

            return student;
        }

        public Student AddStudent(string firstName, string lastName, DateTime? dateOfBirth)
        {
            Student student = new Student()
            {
                FirstName = firstName,
                LastName = lastName,
                DOB = dateOfBirth ?? DateTime.Now
            };

            context.Students.Add(student);
            context.SaveChanges();

            return student;
        }

        public Professor AddProfessor(Professor professor)
        {
            context.Professors.Add(professor);
            context.SaveChanges();

            return professor;
        }

        public Professor AddProfessor(string firstName, string lastName, DateTime? teachingSince)
        {
            Professor professor = new Professor()
            {
                FirstName = firstName,
                LastName = lastName,
                TeachingSince = teachingSince ?? DateTime.Now
            };

            context.Professors.Add(professor);
            context.SaveChanges();

            return professor;
        }
        #endregion

        #region READ
        // Student CRUD
        public Student GetStudent(int id)
        {
            Student student = context.Students
                .Include(s => s.HasProfessors)
                .ThenInclude(p => p.Professor)
                .Where(p => p.Id.Equals(id)).Single();

            return student;
        }

        public IEnumerable<Student> GetStudents()
        {
            IEnumerable<Student> students = context.Students
                .Include(s => s.HasProfessors)
                .ThenInclude(s => s.Professor)
                .ToList();

            return students;
        }

        // Professor CRUD
        public Professor GetProfessor(int id)
        {
            //Professor professor = context.Professors.Find(id);
            Professor professor = context.Professors
                .Include(s => s.HasStudents)
                .ThenInclude(s => s.Student)
                .Where(p => p.Id.Equals(id)).Single();

            return professor;
        }

        public IEnumerable<Professor> GetProfessors()
        {
            IEnumerable<Professor> professors = context.Professors
                .Include(p => p.HasStudents)
                .ThenInclude(p => p.Student)
                .ToList();

            //IEnumerable<Professor> professors = context.Professors.ToList();

            return professors;
        }

        public IEnumerable<StudentProfessor> GetRelationships()
        {
            IEnumerable<StudentProfessor> relations = context.StudentProfessors.ToList();

            return relations;
        }
        #endregion

        #region UPDATE
        public void UpdateStudent(params Student[] students)
        {
            foreach (Student student in students)
            {
                Student fStudent = context.Students.Find(student.Id);

                // Check for name changes
                if (!fStudent.FirstName.Equals(student.FirstName))
                    fStudent.FirstName = student.FirstName;

                if (!fStudent.LastName.Equals(student.LastName))
                    fStudent.LastName = student.LastName;

                // Check for date changes
                if (!fStudent.DOB.Equals(student.DOB))
                    fStudent.DOB = student.DOB;
            }

            context.SaveChanges();
        }

        public void UpdateProfessor(params Professor[] profressors)
        {
            foreach (Professor professor in profressors)
            {
                Professor _professor = context.Professors.Find(professor.Id);

                // Check for name changes
                if (!_professor.FirstName.Equals(professor.FirstName))
                    _professor.FirstName = professor.FirstName;

                if (!_professor.LastName.Equals(professor.LastName))
                    _professor.LastName = professor.LastName;

                // Check for date changes
                if (!_professor.TeachingSince.Equals(professor.TeachingSince))
                    _professor.TeachingSince = professor.TeachingSince;
            }

            context.SaveChanges();
        }
        #endregion

        #region DELETE
        public void DeleteStudent(params Student[] students)
        {
            foreach(Student student in students)
            {
                DeleteRelation(student.HasProfessors.ToArray());

                IEnumerable<Student> _students = context.Students.Where(s => s.Id.Equals(student.Id));

                context.Students.RemoveRange(_students);
            }

            context.SaveChanges();
        }

        public void DeleteStudent(params int[] ids)
        {
            foreach(int id in ids)
            {
                IEnumerable<Student> students = context.Students.Where(s => s.Id.Equals(id));

                foreach (Student student in students)
                    DeleteRelation(student.HasProfessors.ToArray());

                context.Students.RemoveRange(students);
            }

            context.SaveChanges();
        }

        public void DeleteProfessor(params Professor[] professors)
        {
            foreach (Professor professor in professors)
            {
                DeleteRelation(professor.HasStudents.ToArray());

                IEnumerable<Professor> _professors = context.Professors.Where(p => p.Id.Equals(professor.Id));

                context.Professors.RemoveRange(_professors);
            }

            context.SaveChanges();
        }

        public void DeleteProfessor(params int[] ids)
        {
            foreach(int id in ids)
            {
                IEnumerable<Professor> professors = context.Professors.Where(p => p.Id.Equals(id));

                foreach (Professor professor in professors)
                    DeleteRelation(professor.HasStudents.ToArray());

                context.Professors.RemoveRange(professors);
            }

            context.SaveChanges();
        }

        public void DeleteRelation(params StudentProfessor[] studentProfessors)
        {
            foreach(StudentProfessor studentProfessor in studentProfessors)
            {
                IEnumerable<StudentProfessor> relations = context.StudentProfessors.Where(r => r.Equals(studentProfessor));
                context.StudentProfessors.RemoveRange(relations);
            }

            context.SaveChanges();
        }
        #endregion
    }
}
